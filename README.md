# ASCMI - Annales

Dépot Git réunissant les annales des diverses matières du CMI

## Licence 1
##### ***Semestre 1***

|<center>UE</center>|Spécifique|
|:-:|:-:|
|[Analyse 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Analyse%201)|-|
|[Algèbre 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Alg%C3%A8bre%201)|-|
|[Culture et Pratique de l'Informatique](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Culture%20&%20Pratique%20de%20l'Informatique)|-|
|[Conception et Produit Design](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Conception%20&%20Produit%20Design)|-|
|[Algorithmique et Programmation 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Algorithme%20&%20Programmation%201)|-|
|[Méthodologie du Travail Universitaire et PIX](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/M%C3%A9thodologie%20du%20travail%20universitaire)|-|
|[Infographie 3D](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Infographie%203D)|IIRVIJ|

##### ***Semestre 2***
|<center>UE</center>|Spécifique|
|:-:|:-:|
|[Modèles de Calcul](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Mod%C3%A8les%20de%20calcul)|IIRVIJ et ISR|
|[Algèbre 2](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Alg%C3%A8bre%202)|-|
|[Fondements du Calcul et du Raisonnement](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Fondements%20du%20calcul%20et%20du%20raisonnement)|-|
|[Algorithmique et Programmation 2](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Algorithme%20&%20Programmation%202)|-|
|[Programmation Fonctionnelle](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Programmation%20Fonctionnelle)|-|
|[Bases de Données 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Bases%20de%20Donn%C3%A9es%201)|-|
|[Qu'est ce que la connaissance scientifique](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Qu'est%20ce%20que%20la%20connaissance%20scientifique)|IIRVIJ et ISR|
|[Programmation Web 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%201/Programmation%20Web%201)|-|


## Licence 2
##### ***Semestre 3 et 4***

|<center>UE</center>|Spécifique|
|:-:|:-:|
|[Anglais pour l'informatique](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Anglais%20pour%20l'informatique)|-|
|[Architecture des Ordinateurs](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Architecture%20des%20Ordinateurs)|-|
|[Arithmétique et Cryptographie](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Arithm%C3%A9tique%20et%20Cryptographie)|IIRVIJ et ISR|
|[Combinatoires, Probabilités et Statistiques](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Combinatoire,%20Probabilit%C3%A9s%20&%20Statistiques)|-|
|[Logique et Programmation Logique](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Logique%20et%20Programmation%20Logique)|-|
|[Motion Design](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Motion%20Design)|IIRVIJ|
|[Programmation Orientée Objet 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Programmation%20Orient%C3%A9e%20Objet%201)|-|
|[Programmation Orientée Objet 2](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Programmation%20Orient%C3%A9e%20Objet%202)|-|
|[Programmation Web 2](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Programmation%20Web%202)|-|
|[Réseaux IP](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/R%C3%A9seaux%20IP)|-|
|[Structures de Données et Algorithmes 1](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Structures%20de%20Donn%C3%A9es%20et%20Algorithmes%201)|-|
|[Structures de Données et Algorithmes 2](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Structures%20de%20Donn%C3%A9es%20et%20Algorithmes%202)|-|
|[Techniques de Développement](https://gitlab.com/cmi_stg/docs/tree/master/Licence%202/Techniques%20de%20Developpement)|-|

## Licence 3
##### ***Semestre 5 et 6***
|<center>UE</center>|Spécifique|
|:-:|:-:|
|[Economie et Gestion](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/Economie%20et%20Gestion)||
|[Interraction Homme Machine](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/IHM)|-|
|[Intelligence Artificielle](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/Intelligence%20Artificielle)|-|
|[Systèmes Concurrents](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/Syst%C3%A8mes%20Concurrents)|-|
|[Théories des Langages](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/Th%C3%A9orie%20des%20langages)|-|
|[Traitement du Signal](https://gitlab.com/cmi_stg/docs/tree/master/Licence%203/Traitement%20du%20Signal)|-|

***CERDAN Baptiste Responsable Archives***
