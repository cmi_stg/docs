/*CEORRECTION CC1*/
/* 1  (/1,5) */

SELECT nom, prenom, extract(year from sysdate) - extract(year from naissance) AS "Age"
FROM Personnel
WHERE upper(pays) = 'FRANCE';

/* 2  (/1) */

SELECT nom, prenom
FROM Personnel
WHERE no_pers IN (SELECT DISTINCT acteur FROM Casting);

SELECT nom, prenom
FROM Personnel p, Casting c
WHERE p.no_pers = c.acteur;

/* 3  (/1) */

SELECT nom
FROM Personnel
WHERE naissance = (SELECT min(naissance) FROM Personnel);

/* 4 (/1,5) */

SELECT *
FROM Projection
WHERE to_char(date_debut, 'YYYY/MM/DD') = '2016/01/01' AND date_fin IS NULL;

/* 5 (/1,5) */

SELECT nom
FROM Personnel
WHERE no_pers IN (SELECT acteur
					FROM Casting
					WHERE cachet = (SELECT max(cachet)
									FROM Casting));

/* 6 (/2) */

SELECT *
FROM Cinema c, Projection p, Film f
WHERE p.film = f.no_film
AND c.no_cine = p.cinema /*1*/
AND upper(f.titre) = 'BRAZIL'/*0,5*/
AND upper(f.realisateur) = 'TERRY GILLIAM';/*0,5*/

/* 7 (2,5) */

SELECT no_pers, nom
FROM Personnel
WHERE no_pers NOT IN (SELECT DISTINCT acteur FROM Casting)
AND realisateur NOT IN (SELECT DISTINCT realisateur FROM Film);

/* 8 (2,5) */

SELECT f.no_film, count(f.nom) AS "..."
FROM Film f, Projection p
WHERE f.no_film = p.film (+)
GROUP BY f.no_film

/* 9 (2,5) */

SELECT nom
FROM Personnel
WHERE no_pers IN (SELECT acteur FROM Casting GROUP BY acteur HAVING count(*) =
					(SELECT max(count(*)) FROM Casting GROUP BY acteur));

/* 10 (2,5) */

SELECT film
FROM Casting
GROUP BY film
HAVING sum(cachet) = (SELECT max(sum(cachet))
						FROM Casting
						GROUP BY film);